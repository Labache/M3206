#!/bin/sh

# -------------------------------
# Exercice 1.1 : check_internet.sh
# -------------------------------

wget -q --spider http://google.com
# Le -q signifie "quiet", c'est le mode silence
# Le --spider permet de ne pas t�l�charger la page mais juste tester sa pr�sence
echo "[...] checking internet connection [...]"
if [ $? -eq 0 ]; then # Le $? contient la r�ponse de la requ�te pr�c�dente
	echo "[...] Internet access OK [...]"
	exit 0 # Tout s'est bien passé 
else
	echo "[/!\\] No connected to Internet [/!\\]\n[/!\\] Please check configuration [/!\\]"
	exit 1
fi

# -------------------------------
# Exercice 1.2 : update_system.sh  
# -------------------------------

if [ "$(whoami)" = "root" ]; then # On teste si l'utilisateur est le superutilisateur
        echo "[...] loading [...]"
        apt-get update > /dev/null # On renvoie les réponses dan le fichier pseudo-p�riph�rique /dev/nl
        echo "[...] update database [...]"
        echo "[...] loading [...]"
        apt-get upgrade > /dev/null
        echo "[...] upgrade system  [...]"
        exit 0
else
        echo "[/!\\] Vous devez être super-utilisater [/!\\]"
        exit 1
fi

# -----------------------------
# Exercice 1.3 : check_tools.sh
# -----------------------------

tools="git tmux vim htop"

for i in $tools; do
	dpkg -l | grep $i > /dev/null
	if [ $? -eq 0 ]; then
		echo "[...] $i: install� [...]"
	else
		echo "[/!\\] $i: pas installé [/!\\] lancer la commande:\apt-get install $i\`"
	fi
done

# ---------------------------
# Exercice 1.4 : check_ssh.sh
# ---------------------------

dpkg -l | grep ssh > /dev/null                                                                                                       
if [ $? -eq 0 ]; then # On teste si ssh est install�                                                                                                         
	service ssh status > /dev/null 
	if [ $? -eq 0 ]; then # On teste si le démon on ssh est lancé
		echo "[...] ssh: fonctionne [...]"
	else
		echo "[...] ssh: est install� [...]"
		echo "[/!\\] ssh: le service n'est pas lanc� [/!\\]"
		echo "[...] ssh: lancement du service [/!\\] lancer la commande: \`/etc/init.d/ssh start\`" 
	fi
else
	echo "[/!\\] $i: pas installé [/!\\] lancer la commande:\`apt-get instal $i\`"                                        
fi

