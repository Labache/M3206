#/bin/sh!
re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then # On teste si l'argument ajoutépar l'utilisateur n'est pas un nombre
	echo "error: $1 not a number" >&2
else # Si l'argument est bien un nombre
	history | awk '{a[$2]++} END {for (i in a) print a[i], i}' | sort -rn | head -n $1
fi	
